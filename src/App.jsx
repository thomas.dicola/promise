import { useEffect } from 'react';
import './App.css';
import { getProducts } from './service';

// -------------- Promise  -------------- //

// DECLARATION

const demarre = new Promise((resolve, reject) => {
  const isRunning = false;

  if (isRunning === true) {
    resolve();
  } else {
    reject();
  }
});

const calcul = (a, b) => {
  return new Promise((resolve, reject) => {
    const result = a * b;

    if (result < 20) {
      resolve(result);
    } else {
      reject('Resultat trop grand');
    }
  });
};

// UTILISATION

demarre
  .then(() => {
    console.log('good!');
  })
  .catch(() => {
    console.log('Erreur');
  });

calcul(2, 50)
  .then((result) => {
    console.log('Resultat : ' + result);
  })
  .catch((err) => {
    console.log('Erreur : ' + err);
  });

// -------------- ASYNC AWAIT -------------- //

const funcTwo = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('2 secondes');
    }, 2000);
  });
};

const func = async () => {
  console.log('ok');
  const text = await funcTwo();
  return text;
};

func().then((text) => console.log(text));

function App() {
  useEffect(() => {
    getProducts();
  }, []);
  console.log(getProducts());

  return <div className='App'>Exercice sur promise</div>;
}

export default App;
