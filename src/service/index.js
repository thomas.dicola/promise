import axios from 'axios';

export const getProducts = () => {
  return new Promise((resolve, reject) => {
    axios
      .get('https://randomuser.me/api/?nat=fr&results=10')
      .then((response, error) => {
        if (!response || error) {
          return reject(`Response fail ${error}`);
        }
        resolve(response.data);
      });
  });
};

export const login = (username, password) => (dispatch) =>
  axios
    .post('/api/auth/login/local', { username, password })
    .then(() => dispatch(whoami()))
    .catch(() => dispatch(whoami()));
